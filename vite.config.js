import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig(() => {
  return {
    build: {
      outDir: "dist",
    },
    plugins: [
      react(),
    ],
    esbuild: {
      loader: "jsx",
      include: /src\/.*\.jsx?$/,
      exclude: [],
    },
    server: {
      watch: {
        usePolling: true,
      },
      port: 3000,
    },
  };
});

