import { ConfigProvider } from "antd";
import { Provider } from "react-redux";
import { RouterProvider } from "react-router-dom";
import ruRu from "antd/locale/ru_RU";
import { router } from './router/router';
import { Landing } from "./pages/Landing";

function App() {
    return (
      <ConfigProvider locale={ruRu}>
        <Landing />
      </ConfigProvider>  
    );
}

export default App;
