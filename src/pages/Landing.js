import { Button, Flex, Image, Text } from '@chakra-ui/react';
import { Header } from '../components/Header';
import { MainPhoto } from '../components/MainPhoto';
import { BodyInfo } from '../components/BodyInfo';

export const Landing = () => {
    return (
        <Flex
            direction={'column'}
            gap={'24px'}
            mb={'24px'}
        >
            <Header />
            <MainPhoto />
            <BodyInfo />
        </Flex>
    )
}