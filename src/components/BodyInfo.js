import React from "react";
import { Button, Flex, Image, Text } from '@chakra-ui/react';

export const BodyInfo = () => {
    return (
        <Flex
        direction={'column'}
        gap={'24px'}
        mx={'24px'}
        fontFamily={'Arial'}
        >
            <Flex 
                direction={'column'}
                justifyContent={'center'}
                alignItems={'flex-start'}
            >
                <Text
                    fontSize={'18px'}
                    fontWeight={'600'}
                    lineHeight={'1.4'}
                >
                    Маленький ручей с тарзанкой в Кавказских горах
                </Text>
                <Text fontSize={'16px'}>
                    Владимир Кириллов, 2023
                </Text>
            </Flex>
            <Flex 
                direction={'column'}
                justifyContent={'center'}
                alignItems={'flex-start'}
            >
                <Text>
                    Холст, масло и немножко варёной сгущенки
                </Text>
                <Text>
                    55x40 см
                </Text>
            </Flex>
            <Flex 
                direction={'column'}
                justifyContent={'center'}
                alignItems={'flex-start'}
            >
                <Text>
                    Вот эта скамеечка, такая цветная красочка осыпавшаяся... Я нашел ее где-то на улице. С такой облупившейся краской. И я увидел, что это зацепит зрителя как-то. Или вот - слива, с этими капельками - тоже.
                </Text>
            </Flex>
            <Flex 
                direction={'column'}
                justifyContent={'cetner'}
                alignItems={'center'}
            >
                <Button
                    border={'1px solid #4472c4'}
                    borderRadius={'0px'}
                    backgroundColor={'white'}
                >
                    <Text
                        fontSize={'18px'}
                        fontWeight={'600'}
                    >
                        Купить за 33 тыс. ₽
                    </Text>
                </Button>
            </Flex>
        </Flex> 
    )
}