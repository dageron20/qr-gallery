import React from "react";
import { Button, Flex, Image, Text } from '@chakra-ui/react';

export const Header = () => {
    return (
        <Flex
            direction={'row'}
            justifyContent={'center'}
        >   
            <Flex
                direction={'column'}
                color={'#b77c43'}
                mt={'24px'}
            >
                <Flex direction={'row'} gap={'12px'}> 
                    <Image src='logo.png' w={'230px'} h={'auto'}/>
                </Flex>
            </Flex>
        </Flex>
    )
}