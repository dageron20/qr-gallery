import React from "react";
import { Button, Flex, Image, Text } from '@chakra-ui/react';

export const MainPhoto = () => {
    return (
        <Flex w={'100%'} h={'auto'}>
            <Image 
                src='photo1.jpg'
            />
        </Flex>
    )
}